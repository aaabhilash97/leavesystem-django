from rauth import OAuth1Service, OAuth2Service
from leavesystem import settings
from django.http import HttpResponse,Http404,HttpResponseRedirect
import json
class OAuthSignIn(object):
    providers = None

    def __init__(self, provider_name):
        self.provider_name = provider_name
        credentials = settings.config['OAUTH_CREDENTIALS'][provider_name]
        self.consumer_id = credentials['id']
        self.consumer_secret = credentials['secret']

    def authorize(self):
        pass

    def callback(self):
        pass

    def get_callback_url(self):
        return 'https://young-bastion-4000.herokuapp.com'+'/app/callback/'+self.provider_name

    @classmethod
    def get_provider(self, provider_name):
        if self.providers is None:
            self.providers = {}
            for provider_class in self.__subclasses__():
                provider = provider_class()
                self.providers[provider.provider_name] = provider
        return self.providers[provider_name]


class FacebookSignIn(OAuthSignIn):
    def __init__(self):
        super(FacebookSignIn, self).__init__('facebook')
        self.service = OAuth2Service(
            name='facebook',
            client_id=self.consumer_id,
            client_secret=self.consumer_secret,
            authorize_url='https://graph.facebook.com/oauth/authorize',
            access_token_url='https://graph.facebook.com/oauth/access_token',
            base_url='https://graph.facebook.com/'
        )

    def authorize(self):
        return HttpResponseRedirect(self.service.get_authorize_url(
            scope='email',
            response_type='code',
            redirect_uri=self.get_callback_url())
        )

    def callback(self,code):
     try:
        oauth_session = self.service.get_auth_session(
            data={'code': code,
                  'grant_type': 'authorization_code',
                  'redirect_uri': self.get_callback_url()}
        )
        me = oauth_session.get('me').json()
        me['provider']='facebook'
	print me
        return me

     except:
	return None
class TwitterSignIn(OAuthSignIn):
    def __init__(self):
        self.session={}
        super(TwitterSignIn, self).__init__('twitter')
        self.service = OAuth1Service(
            name='twitter',
            consumer_key=self.consumer_id,
            consumer_secret=self.consumer_secret,
            request_token_url='https://api.twitter.com/oauth/request_token',
            authorize_url='https://api.twitter.com/oauth/authorize',
            access_token_url='https://api.twitter.com/oauth/access_token',
            base_url='https://api.twitter.com/1.1/'
        )

    def authorize(self):
        request_token = self.service.get_request_token(
            params={'oauth_callback': self.get_callback_url()}
        )
        self.session['request_token'] = request_token
        return HttpResponseRedirect(self.service.get_authorize_url(request_token[0]))

    def callback(self,code):
     try:
        request_token = self.session['request_token']
        oauth_session = self.service.get_auth_session(
            request_token[0],
            request_token[1],
            data={'oauth_verifier': code}
        )
        me = oauth_session.get('account/verify_credentials.json').json()
	me['provider']='twitter'
	print me
	return me
     except:
		return None
class GoogleSignIn(OAuthSignIn):
    def __init__(self):
        super(GoogleSignIn, self).__init__('google')
        self.service = OAuth2Service(
            name='google',
            client_id=self.consumer_id,
            client_secret=self.consumer_secret,
            authorize_url='https://accounts.google.com/o/oauth2/auth',
    	    access_token_url='https://accounts.google.com/o/oauth2/token',
            base_url='https://accounts.google.com/o/oauth2/auth',
        )
    def authorize(self):
        return HttpResponseRedirect(self.service.get_authorize_url(
            scope='email',
            response_type='code',
            redirect_uri=self.get_callback_url())
        )
    def callback(self,code):
     try:
	data={'code': code,
                  'client_id': self.consumer_id,
                  'client_secret': self.consumer_secret ,
                  'grant_type': 'authorization_code',
                  'redirect_uri': self.get_callback_url(),}
	oauth_session = self.service.get_auth_session(data=data,decoder=json.loads)
	me = oauth_session.get('https://www.googleapis.com/oauth2/v1/userinfo').json()
	me['provider']='google'
	print me
        return me
     except:
		return None
