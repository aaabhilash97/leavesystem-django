from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse,Http404,HttpResponseRedirect
from django.template import RequestContext,loader
from django.core.urlresolvers import reverse
from django.views import generic
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import auth
from datetime import datetime,date
from app.models import leave
from app.validity import datevalid
from app.models import empinfo,leave
from django.conf import settings
from app.oauth import OAuthSignIn
from .slogin import slogin
from django.contrib import messages
# Create your views here.
@login_required(login_url='/app/login')
def manager(request):
   if request.user.is_superuser:
	if request.method=='POST':
		status=request.POST['selectstatus']
		mngdlg=request.POST['mngdlg']
		c=leave.objects.filter(pk=request.POST['id'])
		c.update(aditional_dlg=mngdlg,status=status)
	user=leave.objects.all()
	user=user[::-1]
	return render(request,'app/manager.html',{'leaves':user})
   messages.add_message(request,messages.INFO,'not a super user')
   return HttpResponseRedirect('/app/')
def getleaves(user):
	try:
                if user:
                        user=user.empinfo_set.get(user=user)
                        return user.leave_set.all()
        except:
                return None
@login_required(login_url='/app/login')
def index(request):
	leaves=getleaves(request.user)
	try:
	  if request.method=='POST' and request.POST["addleave"]:
		sdate=request.POST['startdate']
		edate=request.POST['enddate']
		reason=request.POST['reason']
		try:
			emp=request.user.empinfo_set.get(user=request.user)
		except:
			emp=request.user.empinfo_set.create()
			emp=request.user.empinfo_set.get(user=request.user)
		if datevalid(sdate,edate,request.user):
			emp.leave_set.create(start_date=sdate,end_date=edate,reason=reason)
			leaves=getleaves(request.user)
			return render(request,'app/index.html',{'error':'leave application submitted','leaves':leaves})
		else:
			leaves=getleaves(request.user)
			return render(request,'app/index.html',{'error':'can\'t able to allow leave for more than  15 days','leaves':leaves})
	except:
		if request.method=='POST' and request.POST["cancell"]:
			emp=request.user.empinfo_set.get(user=request.user)
			c=emp.leave_set.filter(pk=request.POST['id'])
			c.delete()
	leaves=getleaves(request.user)
	return render(request,'app/index.html',{'error':'','leaves':leaves})
def register(request):
        if request.user.is_authenticated() and request.method=='GET':
                return HttpResponseRedirect(request.GET.get('next') or '/app')
        try:
                if request.POST['register']:
                        try:
                                username=request.POST['username']
                                email=request.POST['email']
                                password=request.POST['password']
                                firstname=request.POST['first_name']
                                lastname=request.POST['last_name']
                                user=User.objects.create_user(username=username,email=email,password=password,first_name=firstname,last_name=lastname)
				user.empinfo_set.create()
                                return render(request,'app/login.html',{'submit':"user registered successfully"})
                        except:
                                return render(request,'app/registration.html',{'submit':"username already in use"})
        except:
                return render(request,'app/registration.html',{})


def login(request):
        if request.user.is_authenticated() and request.method=='GET':
		page='/app'
                if request.user.is_superuser:
                        page='/app/manager'
                return HttpResponseRedirect(request.GET.get('next') or page)
        if request.method=='POST':
                try:
                        username=request.POST['username']
                        password=request.POST['password']
                        user=auth.authenticate(username=username,password=password)
                        auth.login(request,user)
                        try:
                            if request.POST['rememberme']:
                                None
                        except:
                                request.session.set_expiry(0)
			page='/app'
			if request.user.is_superuser:
				page='/app/manager'
                        return HttpResponseRedirect(request.GET.get('next') or page)
                except:
                        return render(request,'app/login.html',{'submit':"invalid user"})
        else:
                return render(request,'app/login.html',{'submit':''})
def logout(request):
        auth.logout(request)
        return HttpResponseRedirect('/app/login')
def oauth_authorize(request,provider):
     try:
        if request.user.is_authenticated() and request.method=='GET':
                raise ValueError
        oauth = OAuthSignIn.get_provider(provider)
        return oauth.authorize()
     except:
        return HttpResponse("<script>window.opener.location.reload(true);window.close();</script>")
#Callback
def oauth_callback(request,provider):
    if request.user.is_authenticated() and request.method=='GET':
                return HttpResponseRedirect('/app')
    if not request.user.is_anonymous():
        return redirect(url_for('index'))
    oauth = OAuthSignIn(provider)
    oauth = OAuthSignIn.get_provider(provider)
    if provider=='google' or provider=='facebook':
                code=request.GET.get('code')
    elif provider=='twitter':
                code=request.GET.get('oauth_verifier')
    try:
    	me = oauth.callback(code)
    	auth.login(request,slogin(me))
    except:
	return HttpResponse("<script>window.opener.location.reload(true);alert('login failed try again');window.close();</script>")
    return HttpResponse("<script>window.opener.location.reload(true);window.close();</script>")
