from django.conf.urls import patterns, url
from app import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^manager/$', views.manager, name='manager'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^register/$', views.register, name='register'),
    url(r'^authorize/(?P<provider>\w+)/$', views.oauth_authorize, name='oauth_authorize'),
    url(r'^callback/(?P<provider>\w+)/$', views.oauth_callback, name='oauth_callback'),
)
