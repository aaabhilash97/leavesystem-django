from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
import datetime
# Create your models here.
class empinfo(models.Model):
        user = models.ForeignKey(User, unique=True)
        email_confirmed=models.BooleanField(default=False)
        provider=models.CharField(max_length=200,default='None')
        propicurl=models.CharField(max_length=200,default='None')
        link=models.CharField(max_length=200,default='None')
        gender=models.CharField(default='None',max_length=200)
        def __str__(self):
               return self.user.username
class leave(models.Model):
	employee = models.ForeignKey(empinfo)
	start_date=models.DateField('start date')
        end_date=models.DateField('end date')
	reason=models.CharField(max_length=200,default="leave application")
	status=models.CharField(max_length=200,default="pending")
	aditional_dlg=models.CharField(max_length=200,default="")
	def expired(self):
		return datetime.date.today()<=self.end_date
