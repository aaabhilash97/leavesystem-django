from datetime import date,timedelta
from app.holidays import pub_holidays
def datevalid(sdate,edate,user):
	s=sdate.split("-")
	e=edate.split("-")
	s=date(int(s[0]),int(s[1]),int(s[2]))
	e=date(int(e[0]),int(e[1]),int(e[2]))
	s_cop,e_cop,n=s,e,0
	while e_cop>s_cop:
		if s_cop.strftime("%A")=='Sunday' or s_cop.strftime("%A")=='Monday' or s_cop in pub_holidays:
			n=n+1
		s_cop+=timedelta(days=1)
	if e-s>timedelta(days=15+n-1):
		return False
	return True
