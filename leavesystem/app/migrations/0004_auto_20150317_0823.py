# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20150317_0809'),
    ]

    operations = [
        migrations.AlterField(
            model_name='leave',
            name='end_date',
            field=models.DateField(verbose_name=b'end date'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='leave',
            name='start_date',
            field=models.DateField(verbose_name=b'start date'),
            preserve_default=True,
        ),
    ]
