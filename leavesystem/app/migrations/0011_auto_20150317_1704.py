# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0010_auto_20150317_1342'),
    ]

    operations = [
        migrations.AlterField(
            model_name='leave',
            name='aditional_dlg',
            field=models.TextField(default=b'', max_length=200),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='leave',
            name='status',
            field=models.CharField(default=b'pending', max_length=200),
            preserve_default=True,
        ),
    ]
