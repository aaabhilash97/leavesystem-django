# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_auto_20150317_0828'),
    ]

    operations = [
        migrations.AddField(
            model_name='leave',
            name='status',
            field=models.CharField(default=b'application submitted', max_length=200),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='leave',
            name='reason',
            field=models.CharField(default=b'application submitted', max_length=200),
            preserve_default=True,
        ),
    ]
