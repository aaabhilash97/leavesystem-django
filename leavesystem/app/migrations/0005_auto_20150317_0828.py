# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('app', '0004_auto_20150317_0823'),
    ]

    operations = [
        migrations.CreateModel(
            name='empinfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email_confirmed', models.BooleanField(default=False)),
                ('provider', models.CharField(default=b'None', max_length=200)),
                ('propicurl', models.CharField(default=b'None', max_length=200)),
                ('link', models.CharField(default=b'None', max_length=200)),
                ('gender', models.CharField(default=b'None', max_length=200)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='leave',
            name='employee',
            field=models.ForeignKey(to='app.empinfo'),
            preserve_default=True,
        ),
    ]
