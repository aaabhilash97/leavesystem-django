# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0011_auto_20150317_1704'),
    ]

    operations = [
        migrations.AlterField(
            model_name='leave',
            name='aditional_dlg',
            field=models.CharField(default=b'', max_length=200),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='leave',
            name='reason',
            field=models.CharField(default=b'leave application', max_length=200),
            preserve_default=True,
        ),
    ]
