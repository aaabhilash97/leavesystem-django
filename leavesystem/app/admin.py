from django.contrib import admin
from app.models import leave,empinfo
from django.contrib.auth.models import User
# Register your models here.
class leaveinline(admin.StackedInline):
        model=leave
	extra=0
class empadmin(admin.ModelAdmin):
        inlines=[leaveinline]
admin.site.register(empinfo,empadmin)

