from django.conf.urls import patterns, include, url
from app import views
from django.contrib import admin
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'leavesystem.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^app/', include('app.urls')),
    url(r'^$', include('app.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
